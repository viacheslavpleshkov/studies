/*
Оператор CREATE INDEX используется для создания индексов в таблицах.
Индексы используются для быстрого извлечения данных из базы данных.
Пользователи не могут видеть индексы, они просто используются для ускорения поиска / запросов.
*/
-- CREATE INDEX Syntax
CREATE INDEX index_name
ON table_name (column1, column2);
-- CREATE UNIQUE INDEX Syntax
CREATE UNIQUE INDEX index_name
ON table_name (column1, column2);
-- DROP INDEX Statement
ALTER TABLE table_name
DROP INDEX index_name;