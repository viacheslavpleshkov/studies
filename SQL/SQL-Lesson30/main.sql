-- Функция MIN () возвращает наименьшее значение выбранного столбца.
SELECT MIN(column_name)
FROM table_name
WHERE condition;
-- Функция MAX () возвращает наибольшее значение выбранного столбца.
SELECT MAX(column_name)
FROM table_name
WHERE condition;
