/*
Ограничение CHECK используется для ограничения диапазона значений, который может быть помещен в столбец.
Если вы определяете ограничение CHECK для одного столбца, оно допускает только определенные значения для этого столбца.
Если вы определяете ограничение CHECK для таблицы, оно может ограничить значения в определенных столбцах на основе значений в других столбцах в строке.
*/
CREATE TABLE table_name (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    CHECK (Age>=18)
);
-- CHECK на ALTER TABLE
ALTER TABLE table_name
ADD CHECK (Age>=18);
-- DROP a CHECK Constraint
ALTER TABLE table_name
DROP CHECK (Age>=18);