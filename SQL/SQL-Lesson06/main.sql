/*
FOREIGN KEY - это ключ, используемый для соединения двух таблиц вместе.
FOREIGN KEY - поле (или набор полей) в одной таблице, которое ссылается на PRIMARY KEY в другой таблице.
*/
CREATE TABLE table_name (
    ID int NOT NULL,
    LastName varchar(255),
    FirstName varchar(255),
    Age int,
    RoleID int,
    PRIMARY KEY (ID)
    FOREIGN KEY (RoleID) REFERENCES Role(ID)
);
-- FOREIGN KEY on ALTER TABLE
ALTER TABLE table_name
ADD FOREIGN KEY (RoleID) REFERENCES Role(ID);
-- DROP a FOREIGN KEY Constraint
ALTER TABLE table_name
DROP FOREIGN KEY RoleID;