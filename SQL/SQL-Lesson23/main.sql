/*
Предложение WHERE используется для фильтрации записей.
Предложение WHERE используется для извлечения только тех записей, которые соответствуют заданному условию.
*/
SELECT * FROM table_name
WHERE condition;

SELECT column1, column2, FROM table_name
WHERE condition;