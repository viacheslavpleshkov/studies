/*
Оператор LIKE используется в предложении WHERE для поиска заданного шаблона в столбце.
В сочетании с оператором LIKE используются две подстановочные знаки:
% - Знак процента представляет нулевой, один или несколько символов
_ - Подчеркнутый символ представляет собой один символ
*/
-- Находит любые значения, начинающиеся с символа "a"
SELECT * FROM table_name
WHERE CustomerName LIKE 'a%';
-- Находит любые значения, заканчивающиеся на "a"
SELECT * FROM table_name
WHERE CustomerName LIKE '%a';
-- Находит любые значения, которые имеют «или» в любой позиции
SELECT * FROM table_name
WHERE CustomerName LIKE '%or%';
-- Находит любые значения, которые имеют «r» во второй позиции
SELECT * FROM table_name
WHERE CustomerName LIKE '_r%';
-- Находит любые значения, начинающиеся с «a» и длиной не менее 3 символов
SELECT * FROM table_name
WHERE CustomerName LIKE 'a_%_%';
-- Находит любые значения, начинающиеся с «a» и заканчивающиеся на «o»
SELECT * FROM table_name
WHERE ContactName LIKE 'a%o';