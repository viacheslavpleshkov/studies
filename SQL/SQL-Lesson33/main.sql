/*
Операторы ANY и ALL используются с предложением WHERE или HAVING.
Оператор ANY возвращает true, если какое-либо из значений подзапроса соответствует условию.
Оператор ALL возвращает true, если все значения подзапроса удовлетворяют условию
 */
-- ANY Syntax
SELECT column_name(s)
FROM table_name
WHERE column_name operator ANY
(SELECT column_name FROM table_name WHERE condition);
-- ALL Syntax
SELECT column_name(s)
FROM table_name
WHERE column_name operator ALL
(SELECT column_name FROM table_name WHERE condition);
