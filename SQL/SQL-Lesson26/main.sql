/*
Оператор IN позволяет указать несколько значений в предложении WHERE.
Оператор IN является сокращением для нескольких условий OR.
*/
SELECT * FROM table_name
WHERE column_name IN (value1, value2);