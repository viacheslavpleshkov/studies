/*
Ключевое слово ORDER BY используется для сортировки результирующего набора в порядке возрастания или убывания.
Ключевое слово ORDER BY по умолчанию сортирует записи по возрастанию.
Чтобы отсортировать записи в порядке убывания, используйте ключевое слово DESC.
*/
SELECT column1, column2,
FROM table_name
ORDER BY column1, column2, ASC;

SELECT column1, column2,
FROM table_name
ORDER BY column1, column2, DESC;