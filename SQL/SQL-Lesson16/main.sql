/*
Оператор ALTER TABLE используется для добавления, удаления или изменения столбцов в существующей таблице.
Оператор ALTER TABLE также используется для добавления и удаления различных ограничений для существующей таблицы.
*/
-- Чтобы добавить столбец в таблицу, используйте следующий синтаксис:
ALTER TABLE table_name
ADD column_name datatype;
-- Чтобы изменить тип данных столбца в таблице, используйте следующий синтаксис:
ALTER TABLE table_name
MODIFY COLUMN column_name datatype;
-- Чтобы изменить название таблицы, используйте следующий синтаксис:
ALTER TABLE table_name
RENAME newtable_name;
-- Чтобы изменить столбец, используйте следующий синтаксис:
ALTER TABLE table_name
CHANGE COLUMN column_name newcolumn_name VARCHAR(50) NOT NULL;
-- Чтобы удалить столбец в таблице, используйте следующий синтаксис:
ALTER TABLE table_name
DROP COLUMN column_name;