-- Предложение HAVING было добавлено в SQL, потому что ключевое слово WHERE не могло использоваться с агрегатными функциями.
SELECT column_name(s) FROM table_name
WHERE condition
GROUP BY column_name(s)
HAVING condition
ORDER BY column_name(s);