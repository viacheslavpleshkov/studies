/*
SQL-псевдонимы используются для предоставления таблицы или столбца таблицы временного имени.
Псевдонимы часто используются, чтобы сделать имена столбцов более читабельными.
Псевдоним существует только для продолжительности запроса.
*/
-- Alias Column Syntax
SELECT column_name AS alias_name
FROM table_name;
-- Alias Table Syntax
SELECT column_name(s)
FROM table_name AS alias_name;
