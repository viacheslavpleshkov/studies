/*
Ограничение PRIMARY KEY однозначно идентифицирует каждую запись в таблице базы данных.
Первичные ключи должны содержать UNIQUE значения и не могут содержать значения NULL.
В таблице может быть только один первичный ключ, который может состоять из одного или нескольких полей.
*/
CREATE TABLE table_name (
    ID int NOT NULL,
    LastName varchar(255),
    FirstName varchar(255),
    Age int,
    PRIMARY KEY (ID)
);
/*
PRIMARY KEY on ALTER TABLE
 */
ALTER TABLE table_name
ADD PRIMARY KEY (ID);
/*
DROP a PRIMARY KEY Constraint
 */
ALTER TABLE table_name
DROP PRIMARY KEY;