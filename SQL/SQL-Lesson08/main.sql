/*
Ограничение UNIQUE гарантирует, что все значения в столбце отличаются.
Ограничения UNIQUE и PRIMARY KEY гарантируют уникальность столбца или набора столбцов.
Ограничение PRIMARY KEY автоматически имеет ограничение UNIQUE.
*/
CREATE TABLE table_name (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    UNIQUE (ID)
);
--UNIQUE Constraint on ALTER TABLE
ALTER TABLE table_name
ADD UNIQUE (ID);
-- DROP a UNIQUE Constraint
ALTER TABLE table_name
DROP UNION ID;