/*
Оператор UNION используется для объединения результирующего набора из двух или более операторов SELECT.
Каждый оператор SELECT в UNION должен иметь одинаковое количество столбцов
Столбцы также должны иметь похожие типы данных
Столбцы в каждой инструкции SELECT также должны быть в том же порядке
*/
-- UNION Syntax
SELECT column_name(s) FROM table1
UNION
SELECT column_name(s) FROM table2;

-- UNION ALL Syntax
SELECT column_name(s) FROM table1
UNION ALL
SELECT column_name(s) FROM table2;
