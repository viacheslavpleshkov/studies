/*
Оператор SELECT используется для выбора данных из базы данных.
Возвращенные данные сохраняются в таблице результатов, называемой результирующим набором.
*/
SELECT * FROM table_name;
SELECT column1, column2 FROM table_name;
