/*
Оператор INSERT INTO SELECT копирует данные из одной таблицы и вставляет ее в другую таблицу.
INSERT INTO SELECT требует, чтобы типы данных в исходной и целевой таблицах соответствовали
Существующие записи в целевой таблице не затронуты
*/
INSERT INTO table2
SELECT * FROM table1
WHERE condition;

INSERT INTO table2 (column1, column2, column3)
SELECT column1, column2, column3 FROM table1
WHERE condition;