<?php

interface FirstInterface
{
    public function getValue();
}

interface LastInterface
{
    public function status();
}

class MyClass implements FirstInterface, LastInterface
{
    public function getValue()
    {
        return 'Hello';
    }

    public function status()
    {
        return 'Ok';
    }
}

$obj = new MyClass();
echo $obj->getValue();
echo $obj->status();
