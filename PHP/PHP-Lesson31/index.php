<?php
//array_chunk — Разбивает массив на части
var_dump(array_chunk(['a', 'b', 'c', 'd', 'e'], 2));
//array_column — Возвращает массив из значений одного столбца входного массива
$records = [['id' => 2135, 'first_name' => 'John', 'last_name' => 'Doe',], ['id' => 3245, 'first_name' => 'Sally', 'last_name' => 'Smith',]];
var_dump(array_column($records, 'first_name'));
//array_combine — Создает новый массив, используя один массив в качестве ключей, а другой для его значений
var_dump(array_combine(['green', 'red', 'yellow'], ['avocado', 'apple', 'banana']));
//array_count_values — Подсчитывает количество всех значений массива
var_dump(array_count_values([1, "hello", 1, "world", "hello"]));
//array_diff_assoc — Вычисляет расхождение массивов с дополнительной проверкой индекса
var_dump(array_diff_assoc(["a" => "green", "b" => "brown"], ["a" => "green", "yellow"]));
//array_diff — Вычислить расхождение массивов
var_dump(array_diff(["a" => "green", "b" => "brown"], ["a" => "green", "yellow"]));
//array_fill_keys — Создает массив и заполняет его значениями, с определенными ключами
var_dump(array_fill_keys(['foo', 5, 10, 'bar'], 'banana'));
//array_fill — Заполняет массив значениями
var_dump(array_fill(1, 10, 'banana'));
//array_flip — Меняет местами ключи с их значениями в массиве
var_dump(array_flip(["oranges", "apples", "pears"]));
//array_intersect_assoc — Вычисляет схождение массивов с дополнительной проверкой индекса
var_dump(array_intersect_assoc(["a" => "green", "b" => "brown", "c" => "blue", "red"], ["a" => "green", "b" => "yellow", "blue", "red"]));
//array_intersect_key — Вычислить пересечение массивов, сравнивая ключи
var_dump(array_intersect_key(['blue' => 1, 'red' => 2, 'green' => 3, 'purple' => 4], ['green' => 5, 'blue' => 6, 'yellow' => 7, 'cyan' => 8]));
