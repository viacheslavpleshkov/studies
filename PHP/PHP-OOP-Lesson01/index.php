<?php

class MyClass
{

}

$product1 = new MyClass;
$product2 = new MyClass;
$product3 = new MyClass;

var_dump($product1 instanceof MyClass);
var_dump($product2 instanceof MyClass);
var_dump($product3 instanceof MyClass);
var_dump($product3 instanceof Card);