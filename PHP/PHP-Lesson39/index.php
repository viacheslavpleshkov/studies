<?php
//Класс mysqli
$mysqli = new mysqli('', 'root', '', 'test') or die("Ошибка " . mysqli_error($link));
//Выполняет запрос к базе данных
$mysqli->query("SELECT * FROM tovars");
//Подготавливает SQL выражение к выполнению
$mysqli->prepare("INSERT INTO city (title, data) VALUES (?,?)");
//Связываем параметры с метками
$mysqli->bind_param("s", $city);
//Запускаем запрос
$mysqli->execute();
//Экранирует специальные символы в строке для использования в SQL выражении, используя текущий набор символов соединения
$mysqli->real_escape_string('Hello');
// Извлекает информацию о последнем выполненном запросе
$mysqli->info;
//Получение информации о текущем состоянии системы
$mysqli->stat();
//Закрывает ранее открытое соединение с базой данных
$mysqli->close();