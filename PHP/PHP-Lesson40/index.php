<?php
$mongo = new MongoClient();
//MongoClient::close — Closes this connection
//MongoClient::connect — Connects to a database server
//MongoClient::__construct — Creates a new database connection object
//MongoClient::dropDB — Drops a database [deprecated]
//MongoClient::__get — Gets a database
//MongoClient::getConnections — Return info about all open connections
//MongoClient::getHosts — Updates status for all associated hosts
//MongoClient::getReadPreference — Get the read preference for this connection
//MongoClient::getWriteConcern — Get the write concern for this connection
//MongoClient::killCursor — Kills a specific cursor on the server
//MongoClient::listDBs — Lists all of the databases available
//MongoClient::selectCollection — Gets a database collection
//MongoClient::selectDB — Gets a database
//MongoClient::setReadPreference — Set the read preference for this connection
//MongoClient::setWriteConcern — Set the write concern for this connection
//MongoClient::__toString — String representation of this connection