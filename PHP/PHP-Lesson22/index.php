<?php
//unset — Удаляет переменную
$unset = 'PHP';
unset($unset);
var_dump($unset);
//empty — Проверяет, пуста ли переменная
$empty = 0;
if (empty($empty)) {
    echo '$var или 0, или пусто, или вообще не определена';
}
//isset — Определяет, была ли установлена переменная значением, отличным от NULL
$isset = 0;
if (isset($isset)) {
    echo '$var определена, даже если она пустая';
}