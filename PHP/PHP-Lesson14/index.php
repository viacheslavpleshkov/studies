<?php
//Функция
function printOne()
{
    $x = 12;
    $y = 35;
    $sum = $x + $y;
    echo $sum;
}

printOne();

//Аргументы функции
function printTwo($x)
{
    echo "$x";
}

printTwo(Hello);

//Возврат значений
function square($num)
{
    return $num * $num;
}

echo square(4);

//Обращение к функциям через переменные
function echoit($string)
{
    echo $string;
}

$func = 'echoit';
$func('test');

//Присвоения анонимной функции переменной
$greet = function ($name) {
    printf("Привет, %s\r\n", $name);
};

$greet('Мир');