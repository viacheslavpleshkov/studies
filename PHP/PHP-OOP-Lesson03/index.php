<?php

class MyClass
{
    public const CONSTANT = 'значение константы';

    function showConstant()
    {
        echo self::CONSTANT;
    }
}

echo MyClass::CONSTANT . "\n";

$classname = "MyClass";
echo $classname::CONSTANT . "\n";

$class = new MyClass();
$class->showConstant();