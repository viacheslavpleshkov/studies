<?php
$a = 5;
$b = 3;
//Равно
var_dump($a == $b);
//Тождественно равно
var_dump($a === $b);
//Не равно
var_dump($a != $b);
//Не равно
var_dump($a <> $b);
//Тождественно не равно
var_dump($a !== $b);
//Меньше
var_dump($a < $b);
//Больше
var_dump($a > $b);
//Меньше или равно
var_dump($a <= $b);
//Больше или равно
var_dump($a >= $b);
//Космический корабль (spaceship)
var_dump($a <=> $b);