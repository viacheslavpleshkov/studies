<?php
//is_array — Определяет, является ли переменная массивом
var_dump(is_array(['это', 'массив']));
//is_bool — Проверяет, является ли переменная булевой
var_dump(is_bool(true));
//is_callable — Проверяет, может ли значение переменной быть вызвано в качестве функции
var_dump(is_callable());
//is_countable — Проверить, что содержимое переменной является счетным значением
var_dump(is_countable([1, 2, 3]));
//is_float — Проверяет, является ли переменная числом с плавающей точкой
var_dump(is_float(27.25));
//is_int — Проверяет, является ли переменная целым числом
var_dump(is_int(2));
//is_iterable — Проверяет, является ли переменная итерируемой
var_dump(is_iterable([1, 2, 3]));
//is_null — Проверяет, является ли значение переменной равным NULL
var_dump(is_null(NULL));
//is_numeric — Проверяет, является ли переменная числом или строкой, содержащей число
var_dump(is_numeric(2));
//is_object — Проверяет, является ли переменная объектом
$object = new Mysqli;
var_dump(is_object($object));
//is_resource — Проверяет, является ли переменная ресурсом
var_dump(is_resource($resource));
//is_scalar — Проверяет, является ли переменная скалярным значением
var_dump(is_scalar('gg'));
//is_string — Проверяет, является ли переменная строкой
var_dump(is_string('Hello'));
