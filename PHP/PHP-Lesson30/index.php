<?php
//strcasecmp — Бинарно-безопасное сравнение строк без учета регистра
if (strcasecmp("Hello", "hello") == 0)
    echo '$var1 равно $var2 при сравнении без учета регистра';
//strcmp — Бинарно-безопасное сравнение строк
if (strcmp("Hello", 'hello') !== 0)
    echo '$var1 не равно $var2 при регистрозависимом сравнении';
//strcoll — Сравнение строк с учетом текущей локали
echo strcmp('a', 'a');
//strip_tags — Удаляет теги HTML и PHP из строки
echo strip_tags('<p>Параграф.</p><!-- Комментарий --> <a href="#fragment">Еще текст</a>', '<p>');
//stripos — Возвращает позицию первого вхождения подстроки без учета регистра
echo stripos('ABCD', 'a');
//stripslashes — Удаляет экранирование символов
echo stripslashes("Ваc зовут O\'reilly?");
//strlen — Возвращает длину строки
echo strlen("Hello word");
//substr — Возвращает подстроку
echo substr("abcdef", 2, -1);
//wordwrap — Переносит строку по указанному количеству символов
echo wordwrap("The quick brown fox jumped over the lazy dog.", 30, "<br />\n");
//strrev — Переворачивает строку задом наперед
echo strrev("Hello world!");
//strstr — Находит первое вхождение подстроки
echo strstr('name@example.com', '@');
//strpbrk — Ищет в строке любой символ из заданного набора
echo strpbrk('This is a Simple text.', 'S');

