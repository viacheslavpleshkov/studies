<?php
//explode — Разбивает строку с помощью разделителя
$explode = explode(" ", "кусок1 кусок2 кусок3 кусок4 кусок5 кусок6");
echo $explode[0]; // кусок1
echo $explode[1]; // кусок2
//htmlentities — Преобразует все возможные символы в соответствующие HTML-сущности
echo htmlentities("A 'quote' is <b>bold</b>");
//html_​entity_​decode
echo  html_entity_decode('A \'quote\' is &lt;b&gt;bold&lt;/b&gt; ');
//htmlspecialchars — Преобразует специальные символы в HTML-сущности
echo htmlspecialchars("<a href='test'>Test</a>");
//htmlspecialchars_decode — Преобразует специальные HTML-сущности обратно в соответствующие символы
echo htmlspecialchars_decode("&lt;a href='test'&gt;Test&lt;/a&gt;");
//implode — Объединяет элементы массива в строку
echo implode(",", array('имя', 'почта', 'телефон'));
//lcfirst — Преобразует первый символ строки в нижний регистр
echo lcfirst('HELLO WORD');
//ucfirst — Преобразует первый символ строки в верхний регистр
echo ucfirst('hello word');
//ucwords — Преобразует в верхний регистр первый символ каждого слова в строке
echo ucwords('hello word');
//strtoupper — Преобразует строку в верхний регистр
echo strtoupper('hello word');
//strtolower — Преобразует строку в нижний регистр
echo strtolower('HELLO WORD');
//ltrim — Удаляет пробелы (или другие символы) из начала строки
echo ltrim("\t\tThese are a few words :) ...  ");
//rtrim — Удаляет пробелы (или другие символы) из конца строки
echo rtrim("\t\tThese are a few words :) ...   ");
//trim — Удаляет пробелы (или другие символы) из начала и конца строки
echo trim("\t\tThese are a few words :) ...   ");

