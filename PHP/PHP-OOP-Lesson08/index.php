<?php

class MyClass
{
    public static $name = 'Slava';

    public static function printHello()
    {
        return 'Hello ' . self::$name;
    }
}

echo MyClass::$public;
echo MyClass::printHello();