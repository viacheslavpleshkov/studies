<?php
//sort — Сортирует массив
$sort = ["lemon", "orange", "banana", "apple"];
sort($sort);
var_dump($sort);
//rsort — Сортирует массив в обратном порядке
$rsort = ["lemon", "orange", "banana", "apple"];
rsort($rsort);
var_dump($rsort);
//arsort — Сортирует массив в обратном порядке, сохраняя ключи
$arsort = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
arsort($arsort);
var_dump($arsort);
//asort — Сортирует массив, сохраняя ключи
$asort = ["d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple"];
asort($asort);
var_dump($asort);
//krsort — Сортирует массив по ключам в обратном порядке
$krsort = ["d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple"];
krsort($krsort);
var_dump($krsort);
//ksort — Сортирует массив по ключам
$ksort = ["d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple"];
ksort($ksort);
var_dump($ksort);
//array_multisort — Сортирует несколько массивов или многомерные массивы
$array_multisort1 = array(10, 100, 100, 0);
$array_multisort2 = array(10, 100, 100, 0);
var_dump(array_multisort($array_multisort1, $array_multisort2));
