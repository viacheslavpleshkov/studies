<?php

abstract class AbstractClass
{
    abstract public function getValue();

    public function printOut()
    {
        print $this->getValue();
    }
}

class MyClass extends AbstractClass
{
    public function getValue()
    {
        return 'Hello ';
    }

}

$obj = new MyClass();
echo $obj->printOut();
