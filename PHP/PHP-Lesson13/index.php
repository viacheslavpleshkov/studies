<?php
//continue используется внутри циклических структур для пропуска оставшейся части текущей итерации цикла и, при соблюдении условий, начала следующей итерации.
for ($i = 0; $i < 10; ++$i) {
    if ($i == 5)
        continue;
    print "$i\n";
}
//break прерывает выполнение текущей структуры for, foreach, while, do-while или switch.
for ($i = 0; $i < 10; ++$i) {
    if ($i == 5)
        break;
    print "$i\n";
}