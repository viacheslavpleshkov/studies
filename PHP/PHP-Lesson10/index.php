<?php
$a = 10;
$b = 5;
//if
if ($a > $b)
    echo "a больше b";
//else
if ($a > $b) {
    echo "a больше, чем b";
} else {
    echo "a НЕ больше, чем b";
}
//elseif
if ($a > $b) {
    echo "a больше, чем b";
} elseif ($a == $b) {
    echo "a равен b";
} else {
    echo "a меньше, чем b";
}