//Добавляет элемент в начало массива: unshift
var array1 = ["Яблоко", "Апельсин", "Груша"];
array1.unshift("Граната");
document.write(array1+"<br>");
//Добавляет элемент в конец массива: push
var array2 = ["Яблоко", "Апельсин", "Груша"];
array2.push("Граната");
document.write(array2+"<br>");
//Удаляет из массива первый элемент: shift
var array3 = ["Яблоко", "Апельсин", "Груша"];
array3.shift();
document.write(array3+"<br>");
//Удаляет последний элемент из массива: pop
var array4 = ["Яблоко", "Апельсин", "Груша"];
array4.pop();
document.write(array4+"<br>");