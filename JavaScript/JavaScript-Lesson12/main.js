//Backspace
alert( 'Привет\bМир' );
//Form feed
alert( 'Привет\fМир' );
//New line
alert( 'Привет\nМир' );
//Carriage return
alert( 'Привет\rМир' );
//Tab
alert( 'Привет\tМир' );
//Символ в кодировке Юникод с шестнадцатеричным кодом
alert( 'Привет\u00A9Мир' );