//Свойство length
var str = "Hello word";
document.write(str.length);
//Метод toUpperCase() возвращает значение строки, на которой он был вызван, преобразованное в верхний регистр.
var str2 = "Hello word";
document.write(str2.toUpperCase());
//Метод toFixed() форматирует число, используя запись с фиксированной запятой.
var n = 12.345;
document.write( n.toFixed(2) ); // "12.35"
document.write( n.toFixed(0) ); // "12"
document.write( n.toFixed(5) ); // "12.34500"
