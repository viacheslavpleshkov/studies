<h1>PHP</h1>
<p>Lesson01 | Hello, world PHP</p>
<p>Lesson02 | $variable, gettype(), echo(), var_dump(), print_r(), print()</p>
<p>Lesson03 | \n, \r, \t, \v, \e, \f, \\, \$, \"</p>
<p>Lesson04 | define/</p>
<p>Lesson05 | +, -, *, /, %, +=, -=, $x++, $x--</p>
<p>Lesson06 | $str_1." ".$str_2."\""."\\"</p>
<p>Lesson07 | &&, and, ||, or, xor, !</p>
<p>Lesson08 |  ==, ===, !=, <>, !==, <, >, <=, >=, <=>/p>
<p>Lesson09 | true, false</p>
<p>Lesson10 | if, else, elseif</p>
<p>Lesson11 | switch</p>
<p>Lesson12 | for, while, foreach, do-while</p>
<p>Lesson13 | break, continue</p>
<p>Lesson14 | function</p>
<p>Lesson15 | array, []</p>
<p>Lesson16 | array(array(),array(),array())</p>
<p>Lesson17 | array_unshift(), array_push(), array_shift(), array_pop(), array_sum(), array_search()</p>
<p>Lesson18 | json_decode(), json_encode()</p>
<p>Lesson19 | global, static</p>
<p>Lesson20 | include, require, include_once, require_once</p>
<p>Lesson21 | form, $_POST, $_GET</p>
<p>Lesson22 | isset(), unset(), empty()</p>
<p>Lesson23 | is_array(), is_bool(), is_callable(), is_countable(), is_float(), is_int(), is_iterable(), is_null(), is_numeric(), is_object(), is_resource(), is_scalar(), is_string()</p>
<p>Lesson24 | abs(), round(), ceil(), floor(), sqrt(), exp(), pi(), pow(), rad2deg(), deg2rad(), max(), min()</p>
<p>Lesson25 | sin(), cos(), tan(), asin(), acos(), atan(), sinh(), cosh(), tanh(), asinh(), acosh(), atanh(), atan2()</p>
<p>Lesson26 | rand(), mt_rand(), getrandmax(), base_convert(), bindec(), decbin(), dechex(), decoct(), hexdec(), octdec()</p>
<p>Lesson27 | addcslashes(), addslashes(), bin2hex(), hex2bin(), chr(), chunk_​split(), convert_​uudecode(), convert_​uuencode()</p>
<p>Lesson28 | explode(), htmlentities(), html_​entity_​decode(), htmlspecialchars(), htmlspecialchars_​decode(), implode(), lcfirst(), ucfirst, ucwords(), strtoupper(), strtolower(), ltrim(), rtrim(), trim()</p>
<p>Lesson29 | money_format(), nl2br(), number_format(), parse_str(), print(), similar_text(), str_pad(), str_repeat(), str_replace(), str_shuffle(), str_split()</p>
<p>Lesson30 | strcasecmp(), strcmp(), strcmp(), strip_tags(), stripos(), stripslashes(), strlen(), substr(), wordwrap(),strrev(), strstr(), strpbrk()</p>
<p>Lesson31 | array_chunk(), array_column(), array_combine(), array_count_values(), array_diff_assoc(), array_diff(), array_fill_keys(), array_fill(), array_flip(), array_intersect_assoc(), array_intersect_key()</p>
<p>Lesson32 | array_keys(), array_merge_recursive(), array_merge(), array_pad(), array_pop(), array_product(), array_push(), array_rand, array_replace_recursive(), array_replace(), array_reverse(), range(), shuffle()</p>
<p>Lesson33 | array_search(), array_shift(), array_slice(), array_splice(), array_sum(), array_unique(), array_unshift(), array_values(), compact(), count(), current(), end(), next(), reset() prev(),extract(), in_array(), key()</p>
<p>Lesson34 | sort(), rsort(), arsort(), asort(), krsort(), ksort(), array_multisort()</p>
<p>Lesson35 | </p>
<p>Lesson29 | time, microtime, mktime, date, getdate, checkdate </p>
<p>Lesson30 | fopen, fwrite, fread, fclose, file_put_contents, file_get_contents, file_exists, filesize</p>
<p>Lesson31 | phpinfo, $_SERVER</p>
<p>Lesson32 | redirect, exit()</p>
<p>Lesson33 | mail</p>
<p>Lesson34 | $_COOKIE</p>
<p>Lesson35 | $_SESSION</p>
<p>Lesson36 | $_FILES</p>
<p>Lesson37 | password_hash(), password_​verify(), password_get_info()</p>
<p>Lesson38 | preg_match(), preg_replace(), preg_split()</p>
<p>Lesson39 | mysqli, query, prepare, bind_param, real_escape_string, info, stat, close</p>
<p>Lesson40 | MongoClient, close, connect, __construct, dropDB, __get, getConnections, getHosts, getReadPreference, getWriteConcern, killCursor, listDBs, selectCollection, selectDB, setReadPreference, setWriteConcern, __toString</p>
<h2>PHP OOP</h2>
<p>Lesson01 | Classes and objects</p>
<p>Lesson02 | Properties</p>
<p>Lesson03 | Class Constants</p>
<p>Lesson04 | Autoloading Classes</p>
<p>Lesson05 | Constructors and Destructors</p>
<p>Lesson06 | Visibility</p>
<p>Lesson07 | Object Inheritance</p>
<p>Lesson08 | Static Keyword</p>
<p>Lesson09 | Class Abstraction</p>
<p>Lesson10 | Object Interfaces</p>
<p>Lesson11 | Traits</p>
<p>Lesson12 | Anonymous classes</p>
<h2>PHP SOLID</h2>
<p>Lesson01 | single responsibility principle</p>
<p>Lesson02 | open/closed principle</p>
<p>Lesson03 | liskov substitution principle</p>
<p>Lesson04 | interface segregation principle</p>
<p>Lesson05 | dependency inversion principle</p>
<h2>PHP Algorithm</h2>
<p>Lesson01 | Bubble Sort</p>
<p>Lesson02 | Insertion Sort</p>
<p>Lesson03 | Selection Sort</p>
<p>Lesson04 | Merge Sort</p>
<p>Lesson05 | Quick Sort</p>
<h2>Java</h2>
<p>Lesson01 | Hellow, word Java</p>
<h1>JavaScript</h1>
<p>Lesson01 | Hellow, word JavaScript</p>
<p>Lesson02 | var, let, const, document.write</p>
<p>Lesson03 | typeof</p>
<p>Lesson04 | +, -, *, /, %, +=, -=, x++, y--</p>
<p>Lesson05 | if, elseif, else</p>
<p>Lesson06 | switch</p>
<p>Lesson07 | for, while, do while</p>
<p>Lesson08 | break, continue</p>
<p>Lesson09 | alert, confirm, prompt</p>
<p>Lesson10 | array, [], length</p>
<p>Lesson11 | pop, push, shift, unshift</p>
<p>Lesson12 | function</p>
<p>Lesson13 | \b, \f, \n, \r, \t, \uNNNN</p>
<p>Lesson14 | abs(), sin(), cos(), tan(), asin, acos(), atan, pow(), exp(), floor(), ceil(), min(), max(), random(), round(), fround(), trunc(), sqrt(), cbrt(), hypot(), sign(), clz32(), imul()</p>
<p>Lesson15 | length, toUpperCase, toFixed</p>
<h2>JavaScript OOP</h2>
<h1>SQL</h1>
<p>Lesson01 | SHOW</p>
<p>Lesson02 | USE</p>
<p>Lesson03 | DESC</p>
<p>Lesson04 | CREATE</p>
<p>Lesson05 | PRIMARY KEY</p>
<p>Lesson06 | FOREIGN KEY</p>
<p>Lesson07 | AUTO INCREMENT</p>
<p>Lesson08 | UNIQUE</p>
<p>Lesson09 | CHECK</p>
<p>Lesson10 | DEFAULT</p>
<p>Lesson11 | NOT NULL</p>
<p>Lesson12 | INDEX</p>
<p>Lesson13 | DROP</p>
<p>Lesson14 | DELETE</p>
<p>Lesson15 | UPDATE</p>
<p>Lesson16 | ALTER</p>
<p>Lesson17 | SELECT</p>
<p>Lesson18 | SELECT DISTINCT</p>
<p>Lesson19 | SELECT TOP</p>
<p>Lesson20 | INSERT INTO</p>
<p>Lesson21 | SELECT INTO</p>
<p>Lesson22 | INSERT INTO SELECT</p>
<p>Lesson23 | WHERE</p>
<p>Lesson24 | AND, OR, NOT</p>
<p>Lesson25 | LIKE</p>
<p>Lesson26 | IN</p>
<p>Lesson27 | BETWEEN</p>
<p>Lesson28 | ORDER BY</p>
<p>Lesson29 | COUNT, AVG, SUM</p>
<p>Lesson30 | MIN, MAX</p>
<p>Lesson31 | AS</p>
<p>Lesson32 | GROUP BY</p>
<p>Lesson33 | ANY, ALL</p>
<p>Lesson34 | HAVING</p>
<p>Lesson35 | UNION</p>
<p>Lesson36 | INNER JOIN</p>
<p>Lesson37 | LEFT JOIN</p>
<p>Lesson38 | RIGHT JOIN</p>
<p>Lesson39 | FULL JOIN</p>
<p>Lesson40 | SELF JOIN</p>